# tasks 1

> NB: please make each task as separate commit!

 1. fork this repo
 1. clone repo locally
 1. optional: create new branch using your name (example: `siim-tiilen`)
 1. fix linting errors present in this repo
 1. create merge request to this repo from your form / branch
 1. add jest to project: 
    - write one test to `sum.js` where inputs are two numbers
       - name all tests `file.spec.js` (example: `sum.spec.js`)
    - fix test script in package.json to run jest for testing
    - NB: ci pipeline should be green after that
 
# tasks 2

> NB: tasks can be done in random order
> NB: please make each task as separate commit!
 
 - add test cases for `sum.js` where input(s) are not numbers and check that error (exception) is given
 - use snapshot testing for `contact.js` testing
 - use mocking to test `time.js` (mock http api call, test from mock that api call was made)
 - BONUS: find at-least one test case that is not tested and not listed in tasks 
 - BONUS: write snapshot serializers for `contact.js` testing to only place userId and name into snapshot 
 - BONUS: write implementation (`repeate.js`) for this test
 
```js
// repeate.spec.js
const repeate = require('./repeate');

test('repeate "a" 3 times', () => {
  expect(repeate('a', 3)).toBe("aaa");
});
```
